-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 20, 2018 at 12:00 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evoainet_evoai`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_mobile_no` bigint(51) NOT NULL,
  `admin_active_inactive` tinyint(4) NOT NULL DEFAULT '1',
  `admin_created_date` date NOT NULL,
  `admin_modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_username`, `admin_password`, `admin_email`, `admin_mobile_no`, `admin_active_inactive`, `admin_created_date`, `admin_modify_date`) VALUES
(1, 'admin', 'admin', '0192023a7bbd73250516f069df18b500', 'admin@gmail.com', 1, 1, '2018-08-08', '2018-08-08');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('169mbu748b1t00dvbosbcq2s2og1foc4', '171.61.22.120', 1539842267, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323236373b5265665f5573657249447c733a333a22313037223b5265665f557365724e616d657c733a383a226268617261743233223b5265665f6c6f676765645f696e7c623a313b5265665f55736572456d61696c7c733a32353a2262686172617463686861627261313340676d61696c2e636f6d223b5265665f657468416464726573737c733a34323a22307866306239383238643566613466633666653535323563363966303838643334653431303165333835223b),
('m3f8dtvl0mrfo9tkvs3p2p20oupnjm2s', '119.207.139.203', 1539842374, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323234383b5265665f5573657249447c733a333a22313133223b5265665f557365724e616d657c733a31333a224a4f204b59554e4720534f4f4e223b5265665f6c6f676765645f696e7c623a313b5265665f55736572456d61696c7c733a31393a226b79756e673434353940676d61696c2e636f6d223b5265665f657468416464726573737c733a34323a22307833663863336334383063303336623164653962393630623933303032343762356430616162393233223b),
('nnb2rr4bftq9h2r81d9h7db61j8h1dvp', '119.207.139.203', 1539842203, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323230333b),
('2kufcjun00b2kp82k682i62n0eo476cq', '171.61.22.120', 1539842561, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323336353b),
('rgqsp91mnihgolel56qu4k2ti8179llb', '194.59.251.91', 1539842541, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323534313b),
('v2f6s9tl0vlkjcf6hh4vsk1sujv07s4n', '194.59.251.91', 1539842541, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323534313b),
('qd8eihqc9ontom958r6rev6vkjtfbk1q', '171.61.22.120', 1539842757, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323735373b),
('uo5feof2rii98m9r7nmeulgd9vgshlt4', '171.61.22.120', 1539843096, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834333039363b),
('lpmkggfhhph57a2r1fgf1vj00b7hhbtg', '211.231.103.91', 1539842786, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323738363b),
('tlll3eotbkug5n5tgjl9q29be9io41m6', '27.0.238.117', 1539842788, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834323738383b),
('hehifbdv1u001lgbd934ms2mbrtchnnf', '171.61.22.120', 1539843434, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834333433343b),
('ed6l7u6pogqb47k0rcgdgs9c6pek7r7o', '171.61.22.120', 1539843737, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834333733373b),
('ra8fudift44v383odqt38vmtl4vmdell', '171.61.22.120', 1539843777, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834333733373b),
('ci0fr80hv844rf9439ci3rvp5i6n2og9', '195.181.174.143', 1539843913, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533393834333931333b);

-- --------------------------------------------------------

--
-- Table structure for table `forgotpassword`
--

CREATE TABLE `forgotpassword` (
  `forgot_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_token` text NOT NULL,
  `created_date` varchar(255) NOT NULL,
  `CreateTime` varchar(255) NOT NULL,
  `ExpiryTime` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forgotpassword`
--

INSERT INTO `forgotpassword` (`forgot_id`, `user_email`, `user_token`, `created_date`, `CreateTime`, `ExpiryTime`) VALUES
(1, 'om.brinfotech@gmail.com', '42555f09a79fcc256d71323bb108d84349', '2018-09-28', '16:03:29', '16:18:29'),
(2, 'om.brinfotech@gmail.com', 'b58a76692f686ae34dc623a3ca3ead0d49', '2018-09-28', '16:07:34', '16:22:34'),
(3, 'promotebalance@gmail.com', '50cd0aaac21a8a9d903b0b33583c325a67', '2018-10-11', '08:14:33', '08:29:33'),
(4, 'castroj1987@icloud.com', '15f3ae2a192f235a792deaaa5c1af11833', '2018-10-11', '08:54:06', '09:09:06'),
(5, 'chimberle@naver.com', '2be0c7a839181f4bae899989997d126347', '2018-10-12', '03:06:26', '03:21:26'),
(6, 'lamhodang2548@hotmail.com', '5f50d00ff414d20911a30b552ffd78a174', '2018-10-12', '10:25:31', '10:40:31'),
(7, 'bharatchhabra13@gmail.com', 'a51194d481c8fa991815ba6b48bf03a1100', '2018-10-12', '11:55:00', '12:10:00'),
(8, 'bharatchhabra13@gmail.com', '3a9b3a56d4b878503c991644520bfcd3107', '2018-10-12', '15:27:31', '15:42:31'),
(9, 'bharatchhabra13@gmail.com', '5e102bd7a33a0cbabbb72a867070648e107', '2018-10-12', '15:29:18', '15:44:18'),
(10, 'karan.brinfotech@gmail.com', '5cf4ec4b849a1a54754c4c0d7b32971e109', '2018-10-12', '16:22:13', '16:37:13'),
(11, 'd9jmy2567@gmail.com', '6dc25c09ff8dea84037b5410344595e3122', '2018-10-13', '15:25:00', '15:40:00'),
(12, 'yoo85082352@gmail.com', 'f30a707c8afdb48291c351611afb2137123', '2018-10-13', '17:10:58', '17:25:58'),
(13, 'shanessy11@gmail.com', 'ec78d1051274e335c0ae09eb22b91a1036', '2018-10-15', '04:36:19', '04:51:19'),
(14, 'promotebalance@gmail.com', 'aa4c5c39e4ac9029fb678300e380c40a67', '2018-10-17', '02:59:53', '03:14:53'),
(15, 'bharatchhabra13@gmail.com', '09fb3dae14333a3e8dcf27901c6e61ff107', '2018-10-18', '14:47:50', '15:02:50'),
(16, 'shanessy11@gmail.com', '595803f41b229bc5213b494f7763cd7436', '2018-10-18', '15:17:57', '15:32:57'),
(17, 'bharatchhabra13@gmail.com', '8de0259c13b3d5339ff87ffeb54f05b5107', '2018-10-20', '10:07:20', '10:22:20');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `user_id`, `subject`, `email`, `message`, `status`, `modify_date`) VALUES
(1, 1, 'Test', 'bharatchhabra13@gmail.com', 'Thsi is for test', 0, '2018-10-09 13:43:02'),
(2, 1, 'Testing', 'bharatchhabra13@gmail.com', 'This is for test', 1, '2018-10-09 16:39:08'),
(3, 1, 'testing 10Oct2018', 'bharatchhabra13@gmail.com', 'This is for test', 0, '2018-10-10 03:00:44'),
(4, 1, 'testing 10Oct2018', 'bharatchhabra13@gmail.com', 'This is for test', 0, '2018-10-10 03:00:49'),
(5, 62, 'This is for test support ', 'om.brinfotech@gmail.com', 'This is support test message', 0, '2018-10-10 09:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `support_chat`
--

CREATE TABLE `support_chat` (
  `id` int(11) NOT NULL,
  `ticket` varchar(255) NOT NULL,
  `to_id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support_chat`
--

INSERT INTO `support_chat` (`id`, `ticket`, `to_id`, `from_id`, `message`, `status`, `created_date`) VALUES
(1, '1', 1, 0, 'Thsi is for test', 0, '10-10-2018'),
(2, '1', 0, 1, 'cfghfghgfh', 0, '10-10-2018'),
(3, '2', 1, 0, 'This is for test', 1, '10-10-2018'),
(4, '2', 0, 1, 'Hello', 1, '10-10-2018'),
(5, '2', 0, 1, 'Hiiiiiii', 1, '10-10-2018'),
(6, '3', 1, 0, 'This is for test', 0, '10-10-2018'),
(7, '4', 1, 0, 'This is for test', 0, '10-10-2018'),
(8, '4', 0, 1, 'Hello Bharat,', 0, '10-10-2018'),
(9, '4', 0, 1, 'How are you?', 0, '10-10-2018'),
(10, '4', 0, 1, 'How are you?', 0, '10-10-2018'),
(11, '4', 1, 0, 'I am fine', 0, '10-10-2018'),
(12, '4', 1, 0, 'hello', 0, '10-10-2018'),
(13, '4', 0, 1, 'this is chat', 0, '10-10-2018'),
(14, '4', 0, 1, 'this is for another test', 0, '10-10-2018'),
(15, '4', 0, 1, 'kjkljkjklj', 0, '10-10-2018'),
(16, '4', 0, 1, 'syrytrtyrty', 0, '10-10-2018'),
(17, '4', 0, 1, 'hgjjhghj', 0, '10-10-2018'),
(18, '4', 0, 1, '45555555555555', 0, '10-10-2018'),
(19, '4', 0, 1, 'rtyrtyrtyrty', 0, '10-10-2018'),
(20, '4', 0, 1, 'ffffff', 0, '10-10-2018'),
(21, '4', 1, 0, 'dfghdfghdgfh', 0, '10-10-2018'),
(22, '4', 0, 1, 'dftgdfg', 0, '10-10-2018'),
(23, '4', 1, 0, '545454654', 0, '10-10-2018'),
(24, '4', 0, 1, 'sdgdsfgdg', 0, '10-10-2018'),
(25, '4', 1, 0, '5454', 0, '10-10-2018'),
(26, '4', 1, 0, '5454', 0, '10-10-2018'),
(27, '4', 0, 1, 'adfdfg', 0, '10-10-2018'),
(28, '4', 1, 0, 'fghfghfghfgh', 0, '10-10-2018'),
(29, '4', 0, 1, '123essd', 0, '10-10-2018'),
(30, '4', 1, 0, 'w455345tt', 0, '10-10-2018'),
(31, '4', 1, 0, 'w455345ttfgh', 0, '10-10-2018'),
(32, '4', 1, 0, 'fghgfh', 0, '10-10-2018'),
(33, '4', 1, 0, 'fgdfgdfg', 0, '10-10-2018'),
(34, '4', 1, 0, 'shgdtghfgh', 0, '10-10-2018'),
(35, '5', 62, 0, 'This is support test message', 0, '10-10-2018'),
(36, '4', 0, 1, '78456', 0, '10-10-2018'),
(37, '5', 62, 0, '', 0, '10-10-2018'),
(38, '5', 62, 0, 'tyutyu', 0, '10-10-2018'),
(39, '5', 62, 0, 'fgdfg333', 0, '10-10-2018'),
(40, '5', 0, 62, 'dgdfg', 0, '10-10-2018'),
(41, '5', 62, 0, 'sdf', 0, '10-10-2018'),
(42, '5', 0, 62, 'drg', 0, '10-10-2018'),
(43, '4', 0, 1, 'dfgfg', 0, '11-10-2018'),
(44, '4', 0, 1, 'juhjjjjjjj', 0, '11-10-2018'),
(45, '1', 0, 1, 'syrytrtyrty', 0, '11-10-2018'),
(46, '1', 0, 1, 'fffffffffffffffffffff', 0, '11-10-2018'),
(47, '6', 109, 0, 'I want to change my ETH address', 1, '12-10-2018'),
(48, '6', 0, 109, 'dfsfsdf', 1, '12-10-2018'),
(49, '6', 109, 0, 'asfsfsfsfsf', 1, '12-10-2018'),
(50, '6', 109, 0, 'asfsfsfsfsf', 1, '12-10-2018'),
(51, '6', 109, 0, 'ffyfytfyt', 1, '12-10-2018'),
(52, '6', 0, 109, 'sdffdsafs', 1, '12-10-2018'),
(53, '6', 0, 109, 'sdffdsafs', 1, '12-10-2018'),
(54, '6', 0, 109, 'sfsafdsf', 1, '12-10-2018'),
(55, '7', 107, 0, 'This is for test demo  support section', 0, '15-10-2018'),
(56, '7', 0, 107, 'ok i will', 0, '15-10-2018'),
(57, '7', 107, 0, 'please do', 0, '15-10-2018'),
(58, '7', 0, 107, 'ok', 0, '15-10-2018'),
(59, '7', 0, 107, '', 0, '15-10-2018'),
(60, '7', 0, 107, '', 0, '15-10-2018');

-- --------------------------------------------------------

--
-- Table structure for table `sys_sessions`
--

CREATE TABLE `sys_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_fname` varchar(255) NOT NULL,
  `user_lname` varchar(255) NOT NULL,
  `user_image` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_referral_code` varchar(255) NOT NULL,
  `user_eth_address` text,
  `user_residence_country` varchar(255) DEFAULT NULL,
  `user_citizenship_country` varchar(255) DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `otp` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `otp_login_code` varchar(42) DEFAULT NULL,
  `otp_backup_codes` varchar(384) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `isActive` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `sign_in` tinyint(4) NOT NULL DEFAULT '1',
  `profile_update` tinyint(4) NOT NULL DEFAULT '1',
  `password_update` tinyint(4) NOT NULL DEFAULT '1',
  `transaction_approval` tinyint(4) NOT NULL DEFAULT '1',
  `mailSent` tinyint(4) DEFAULT NULL,
  `verify_status` tinyint(4) NOT NULL DEFAULT '0',
  `user_token` longtext,
  `ExpiryTime` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_fname`, `user_lname`, `user_image`, `user_email`, `user_password`, `user_referral_code`, `user_eth_address`, `user_residence_country`, `user_citizenship_country`, `ip_address`, `activation_code`, `otp`, `otp_login_code`, `otp_backup_codes`, `remember_code`, `salt`, `last_login`, `isActive`, `active`, `sign_in`, `profile_update`, `password_update`, `transaction_approval`, `mailSent`, `verify_status`, `user_token`, `ExpiryTime`) VALUES
(1, 'Bharat', '', '', 'om.brinfotech@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', 'PEfkBBMR5Di0x/tRnRCw+WdaT2jvdjw2hDk6R39T4W1PnsL7QdsV4kqh1/nTtcm3h1KCA8d1SMG4TejDbQULvQ==', '20X25vbW1XBJ/GsEWdnRf.23862b69620308ed83', 'qIoLeoTEJ67oFisWvpwfoYlGUcQ/+Kc+bZU1a3iLO2UYjur6fGqo9Y/5lnobT/VbuTudB6SyQTah9klCrMbeF38gJAWGefBzsir15mkFCdMiCs/Xxun4Gj5sjJoDZVCCQGYjbwjACKCZlu2xjCA4Pc/OXA9zEQqJh0Matgf2qUEwV2rM9qb9qhEP2IJjTQnFdpEaUjsDX7I9dqwWmS/XNYcWgenuNXFa9K+aGN7ABdPh4O4OzitWqEuqS76U9BewHaoJszDxHmwFKCASIV/s/NNMMWmCtJx3K5dV9Z18Up49ziNI7274gBzvFqBJqKxytTN9tlj44ARpWe1HKqruPTuVjhBtwwrxsu0b3g7zFoL3XqyHxGh29Neie3RnwszB', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 1, '', ''),
(2, 'Bharat', 'chhabra', '', 'bharatchhabra13@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(3, 'Invest101', '', '', 'lamhodang2548@hotmail.com', 'b83faadf0d3b9a031f99fb88789b84f1', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(4, 'Bag', 'Collector', '', 'jmccool89@gmail.com', '3563c3b67b60f8cd02b6e7fc178b7407', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(5, 'Milo_82', '', '', 'impexbooties@gmail.com', 'f9265b1c2f71a7396e0aae239c1a6bd6', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(6, 'cryptoT', '', '', 'cryptoapparel4u@gmail.com', 'a7c332b383f22008b3471227a1812176', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(7, 'JangoFett', '', '', 'brennie_08@live.com.au', '980d9499d2ddcf6fbd858e77be3cd893', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(8, 'Alex Diaz', '', '', 'CryptoAlexDiazJr@gmail.com', 'b633ca0a43af4306913bb28614462c77', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
(9, 'KoF', '', '', 'HyperHyips@gmail.com', '90add6e39020f9156e90d64791ca1cec', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `otp` varchar(255) DEFAULT NULL,
  `otp_login_code` varchar(40) DEFAULT NULL,
  `otp_backup_codes` varchar(384) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `eth_address` varchar(255) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `sign_in` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED DEFAULT '0',
  `verify_status` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `user_referral_code` varchar(255) NOT NULL,
  `user_referenced_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `otp`, `otp_login_code`, `otp_backup_codes`, `salt`, `email`, `eth_address`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `sign_in`, `active`, `verify_status`, `first_name`, `last_name`, `company`, `phone`, `user_referral_code`, `user_referenced_code`) VALUES
(33, '', 'Joe', '5c096a192b2b7d8b0a56c13a4d8ebad5', NULL, NULL, NULL, NULL, 'castroj1987@icloud.com', '0x6B9E9E10aBB661B56b0602817c3F4bCD7f4D32c2', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '3ecfb8a8f5ada39497b1ca4358d1c47311b8055d', '0'),
(34, '', 'GeorgeN1', '17e0e4aea6cd6f3953206a6f57fd8fd5', NULL, NULL, NULL, NULL, 'geoneo@iafrica.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'a68026fc666384f098c8046c2dd0fc35b9f204b5', '0'),
(35, '', 'tony', '7c3fe38a88ffa0a264c230c0e99c997e', NULL, NULL, NULL, NULL, 'morrisholding@orcon.net.nz', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '5cb9d93aab10db5667669a0efcd387f19cabfb02', '0'),
(36, '', 'shanessy', 'd5599dabf47baf30e05d3e791513354c', NULL, NULL, NULL, NULL, 'shanessy11@gmail.com', '0xE64623F6F4ac3CEB488D2E884a12e94458A29b17', NULL, NULL, NULL, NULL, 0, 1539856388, 0, 1, 1, NULL, NULL, NULL, NULL, '67eb49fb28b3ce93eb414e31d3c6e0409d3b2e7d', '0'),
(37, '', 'CryptoTiwi', 'f822bf4f1c962d5052bcde443afb8094', NULL, NULL, NULL, NULL, 'Cryptotiwi@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'a9592f622812af0d41cccfe116ad04264b90869c', '0'),
(38, '', 'Calais250', 'e45a4bbf258e0590bee162a349557dba', NULL, NULL, NULL, NULL, 'tamle250@yahoo.com.au', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '80a24a38dd4ecfb512d999d103e9cdd8b7b99026', '0'),
(39, '', 'Kasper Lundqvist', '804d4a425dc4aef7aa66577499c5f77d', NULL, NULL, NULL, NULL, 'alexnr1000@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'f27b895c82fd966f4c99b99ce38d1825128dd933', '0'),
(40, '', 'Andrew', '4d58d9fa6b555c8075c460f02d245724', NULL, NULL, NULL, NULL, 'newrossman@yahoo.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '2429fb0d2a76d75f67a7ce3e88952374ba57996e', '0'),
(41, '', 'Gaia', 'b2daf1e200d020062940250c3b5fbef8', NULL, NULL, NULL, NULL, 'gaiaeconomy@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '62c424116c80ea72c432936ba10f8e79832eb10a', '0'),
(42, '', 'CHANDEL', '3ef745d422d389a287679f42100fdadd', NULL, NULL, NULL, NULL, 'mridul7070@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '49e63b3b54ae3dbfc01c4a117b780e14a0c3da82', '0'),
(43, '', 'oren730', '36cf869a174544a17b5b0b8a1e5eb8da', NULL, NULL, NULL, NULL, 'networking9@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '8deda04b3b4afb43e290e79f2fa5a4c9584bf3f1', '0'),
(44, '', 'sirpixalok', '9ac4c9a9b282a46b9afe10ae03acd6e6', NULL, NULL, NULL, NULL, 'rhodesn81@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '14ddc240b289dd831b9d269491d589fc9acb8723', '0'),
(49, '', 'Johan', '34e3a6b0939e553d02f3ac91fcbae0d0', NULL, NULL, NULL, NULL, 'catry.johan@telenet.be', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'addb43f8fb1fa079d5bc88bae3b7c54fc07db64c', '0'),
(50, '', 'JO KYUNG SOON', '728392308391a605dd3191d85e9756bd', NULL, NULL, NULL, NULL, 'sson195859@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '224fdd42d344c0cf96589ad785b3adcf8c9e4a46', '0'),
(51, '', 'Clover', '3c77e67046493426c5dd40506b005fda', NULL, NULL, NULL, NULL, 'cryptoclover@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'a22db7a1edc0a5c6fe4819615de8291fa2992c65', '0'),
(52, '', 'Barry Sauer', '82d6a2bc950e94cf09857981618b4f34', NULL, NULL, NULL, NULL, 'rigga88@bigpond.com', '0xdbEC81b4Eab0B5234dBd9ff7c456BF750d7a9086', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '2df355a8d9293077fea132b708532f37755676ff', '0'),
(53, '', 'Eric Raoul Njiosseu Tankoua', '68d7903e5fddca698b2116e0c7fdff0f', NULL, NULL, NULL, NULL, 'crypto@et-tours.de', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'cbab272f71b364d9b65ac3874573834643cefc4a', '0'),
(54, '', 'fractal', '26fdc47f9dd4a7de7cb47c5b724b0c2d', NULL, NULL, NULL, NULL, 'info@appx.hk', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '2116338818ec74bff92f2f026f59de8100351e66', '0'),
(55, '', 'AppX Matthew', '26fdc47f9dd4a7de7cb47c5b724b0c2d', NULL, NULL, NULL, NULL, 'matthewcheng2002@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'f26c9c27f85698d08f97275cd927f132ba38dacc', '0'),
(56, '', 'sonar', '7a1012062000e417cf37903e1b6329c3', NULL, NULL, NULL, NULL, 'pobs279@gmail.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '53f1756d6164ad5f4160aaa443abc3b4b15d9212', '0'),
(57, '', 'Tim Madden', 'cde30f7857608232f675b80f391d6b35', NULL, NULL, NULL, NULL, 'demc564warb@outlook.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'cb26a5833aab8be3acb791a3011b05bafbd9b6db', '0'),
(58, '', 'David Franks', '3e845eaedfefd9bd67497902034d977d', NULL, NULL, NULL, NULL, 'bmwdream@yahoo.com', '', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '73078dee80ebb4951c8e7609d78b54dbf0343863', '0'),
(63, '', 'EVOAI', '984bdda44d065bb6fe949e0b8f10e17f', NULL, NULL, NULL, NULL, 'EVOAINETWORK@GMAIL.COM', '0xa75c84d2a32cd53e289dff512e6b4530a2b6ab2a', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '03677d5b29ab70b6d3ee70de8f9a076fa16cac40', ''),
(67, '', 'Matt Rossi', 'e24c4950e6e2090d1dcc881853ddcf1f', NULL, NULL, NULL, NULL, 'promotebalance@gmail.com', '0x4f50Dd42D3182A7AE0bF715cDc0C5196a03364FA', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'a3499c58e9793ed569f7dc8490677024ff80b05e', '0d6a56948e0d2ca27a5b723256ca31a168217c2b'),
(72, '', 'Taurus', 'a363111ddb04c54fae9758a9713e769f', NULL, NULL, NULL, NULL, 'taurusheat@live.co.za', '0x9dfbBD38792B4430E8ec1E06B84A399475a8035f', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '961295b5d778a5901801f6762488d7c9d8229912', '0d6a56948e0d2ca27a5b723256ca31a168217c2b'),
(74, '', 'lanu', '4fcf1bb1ba29a2c200d21963e1aaa041', NULL, NULL, NULL, NULL, 'lamhodang2548@hotmail.com', '0xE6eB0373B0CC1C21eA7D7D8d8f102205b7338C32', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'ce87e23f3b9f4ceeec98a8fb836c528dd411eb4b', ''),
(75, '', 'invest101', '4fcf1bb1ba29a2c200d21963e1aaa041', NULL, NULL, NULL, NULL, 'nooodlecup@gmail.com', '0x6E46eD74A9A6EAf6C6755F26c20853D8c69de5E7', NULL, NULL, NULL, NULL, 0, 1539877570, 0, 1, 1, NULL, NULL, NULL, NULL, '6bbe668e4a9f5f66f0e75ac34fc400f90b0fb6d6', ''),
(76, '', 'JangoFett', '980d9499d2ddcf6fbd858e77be3cd893', NULL, NULL, NULL, NULL, 'brennie_08@live.com.au', '0xba507528A968ca351161438132272Ad3B48A0F62', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '4bfa1655b2c9974195b6c8f76cafd65ece792bee', '6bbe668e4a9f5f66f0e75ac34fc400f90b0fb6d6'),
(77, '', 'Charlie', '8e3186560302d27b3e7c5d960302c7dc', NULL, NULL, NULL, NULL, 'charlieb5280@gmail.com', '0x3FFE53eC396be473da41cDC9eadea732F5777F52', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '9c75aba6d07310cc2e49164b9dde26f49aef18c1', '6bbe668e4a9f5f66f0e75ac34fc400f90b0fb6d6'),
(78, '', 'Yrelb', 'ed69e04b40aac8c968903283fd1ac945', NULL, NULL, NULL, NULL, 'yrelb.tg@gmail.com', '0xFeb1BEb653b1606cE30DCaf96824372d61Cc73A7', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'c1e43b0243e584ac9e6452200cbc34372e7ff692', '6bbe668e4a9f5f66f0e75ac34fc400f90b0fb6d6'),
(79, '', 'Val Valiant Thor', '816c2be0225fe87b7fe2265fbc748b91', NULL, NULL, NULL, NULL, 'CryptoAlexDiazJr@gmail.com', '0x008ca4f1ba79d1a265617c6206d7884ee8108a78', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'de4b054b56744882c2e40a09ee37d525c438b36e', '6bbe668e4a9f5f66f0e75ac34fc400f90b0fb6d6'),
(80, '', 'Felix', 'ecec947ba031ff44b4e199f4694909db', NULL, NULL, NULL, NULL, 'rvs-felix@gmx.de', '0x364370c1a19cf4a58b3505d3cb1826a0bb9ece8b', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '7842d237f4ee29ead7ffadd964ec87e7da67f117', 'c1e43b0243e584ac9e6452200cbc34372e7ff692'),
(81, '', 'peter pan', 'b633ca0a43af4306913bb28614462c77', NULL, NULL, NULL, NULL, 'Alex.Diazjr@hotmail.com', '0x8da497d03a58cd76e1cecea8dd528e16dae6bbdf', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'cf88f0587e842b7b7bac8f0de9f737d2c79443c8', ''),
(82, '', 'JustinLim', '17789dabee2d0f42545a05ce52873f25', NULL, NULL, NULL, NULL, 'algorithmictrader606@gmail.com', '0xe5Bc7f098b58e61d679fb1908e4BA40AC7477C76', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '32fcb3147b2ecdf93df25c0f1a8a858ad61aec66', '6bbe668e4a9f5f66f0e75ac34fc400f90b0fb6d6'),
(83, '', 'Hyun doowhan', 'f0191abb3af70d32c1aef04c437c922b', 'BxOOpBFPyokkkhpsBACvqk9cQn9H72yZqZUvtjVAQ5omEzkgKL9UvwDTs7HzZg2eJCk6zpTnoGzpsjAvMVaKTA==', NULL, 'kclDWPYwXIFWDRgzRL4dDERUv85kY79LgDMTG/EO9G8dyjO/N822Qdg5AZ6banPWiV0MaxfEeGMamjl97EKS++bvglYDdsuQyKd5ysB2EIxFzeAQsKFHExqP6iD/0ZjP03rrm3EHCryJtA376GouY1wRnABHRxqj1rCCHOWLh7WLzC2pgMpQOuMWxboO51/lIG7JOIMg979aPBGoFEqW/1H9fd4Z3yVdkAjwuNNhQK7addsECWWfzLZKi91MFnjVNv22xKrM56A2RiZeA+0s9Sm8vMxDtdBC77SBpuB3tymAGNyBsPLgyJirL1yaEfwZfsUAk44iJPYLkLnB5C3kJ/QhuqAzeryi1A3XZ3UkF9dQClt8Y9cMUY5zKe0IiIDf', NULL, 'Storm2400@naver.com', '0x779BBf96Ce22B534634F833da71878dffeEB82dB', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, '3369b8c0d6667a1b8fbd99fbdf36d2a5f96e999b', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(84, '', 'cryptomad', '69cd8d6bba14a5a4122de3fdaea4bf54', NULL, NULL, NULL, NULL, 'madwrex30@yahoo.com', '0x1Cd54d41b7287b2C7794c5ddc9b66911865724F0', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'e19fe9664b251a651d7a879493dc80646169aa5f', '7842d237f4ee29ead7ffadd964ec87e7da67f117'),
(85, '', 'choi1318', '0ee9a987b01350bf0f8330e7bcb37196', NULL, NULL, NULL, NULL, 'choi1318862@gmail.com', '0x54604708ebAB4871Cfa9eEC44Ab6B20F014d2e11', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '242556619896adbb1375e45c31b4eb08c4d73651', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(86, '', 'haowner', 'f2b17f1e413f89fba19c7e87693a441d', NULL, NULL, NULL, NULL, 'goodworld7925@gmail.com', '0xc6aea8bc79bcb8d7849bd3f089d1878ab3c4a3cf', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'a55ade8ca4daa775e605e11a17efccbb5e3a4dfd', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(87, '', '홍현기', 'ffa541d4464eb2280a68aa6dca742005', NULL, NULL, NULL, NULL, 'fkfldi74@naver.com', '0x17B2A55279DEEB35a55458D1AbF496E7204903cA', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'fcc9b66d897f4b240f5a1a3243819c4958cfc60b', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(88, '', 'livingstone', '2218a7ec6b0c832cb0ddc6c53bd6e075', 'yEDBUkXNbttqWYKLs31M+JG+mHnRXS9QAvHm60jAd6wYgIU6uVVViNMOrxfVQ8wL5wkHSYHEcFxbSsvH8ldcdA==', 'GfUsipmgExWqqbr74iKogu55f4a0a5a495bc62f5', 'r1kIDsbJ4KO8E5dHBHbKrkyDxIhTBSNAmCpEfY7ioNZ72MrTf0egLasQ5S2u90roSpo0PxXZhVLRLaeKFEvvUlGOO7CIXwUICilUYteLrUsYcC/phPcVvl50lsPsAHvgn35ptkQRgWleeW1EJh4FMo6nRqZ4VTl04vI7TQ6a44aTuQDsni3MtkzTR8FFVXkgnstVgXSTiFNhLiIaxLgj+TUdgJpZYZ2tf4RAju+kS13KqgXwjLcOcE6kleJee8WB4rV0R3PGAauFVLPyntpME9gUz46iuRq/YjAvvClg2kkQ/pNOcIzO/xX7vSmBiZOoo5Eq9pxip2x+fv3h+0T/nEN8i5N77+RHZU4gn9I+rTibp+V5AcnlHifnOHZk4Wlq', NULL, 'suhyoon316@gmail.com', '0x793df82f5179c2f70d3e3bab0a6dae8ee06ad83d', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, 'cb7075e9167fe4cd26f62bfda2d84743b5cf665b', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(89, '', 'Kimyutak', 'c21a698657eb52d542f5dbc4d2cefbd5', NULL, NULL, NULL, NULL, 'kytak3533@daum.net', '0xf55f779Cd8717623d062E029e9a6d7f2F182B4B5', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '9b777c5eea1d93ee539f76018560c70ad19abc36', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(90, '', 'ryutop', 'fe497017221216b5a612c24b59b0a81a', NULL, NULL, NULL, NULL, 'ryujunhee00@gmail.com', '0xBE39db8e5C9498AA97DF91677Fc04E85D6532BA9', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '2ce7fed6704a7c1962b064fece1b46f0ad5b0637', 'a55ade8ca4daa775e605e11a17efccbb5e3a4dfd'),
(91, '', 'solgasil', 'a7e41ae9b37bf9b6cf10b069f8c51824', '8aZVIqRCKZKojThflG5UPcayX4RCdUU507RGC9qkAQS/xNxonzQL8ihzuHYoC6HsoHm9Qzc39Jrlf4d21K8a4g==', 'Ltk4J5WmN2zAI5osI/IjBu0aac04311bcc6d6ec0', '7lcALZtaaqftKN7B24qOsIBWvXfvCkvSKzrBJ82d6vbng+Su571OOxDhcv/3ivFqpoNafYqUYi6jRVIVGeeaxs05kB4BOL/w0QsPV58Xm8dmJ/duosXKA4lD8qB6bXo47wuC0HGGVAqC/zlNAlAal8GTBCkXc2PEEVrL2cw2cOl07iHS9O9aPYRdPB7i412R1NQzQbiJ2UiVOnmj4mCjc/NRB6r9CIesuaDCMYZi+Kh7UTXlJMmsxJpA4NSB7NmKQfwa7GViYOmGG1nmSSY6Zwroi8zlgVn8io7Fy4qfPGzPQRxschuIHYe8nK6wWlfFhC8hLYdwv7DciNam+TZ+2WPhfY6cVMMWCpl7JCtjBNkDMd21sd5t6woALT0S5omN', NULL, 'jung641024@gmail.com', '0xe3d1aea4f943c451e98e6d645bad16f332d3ae47', NULL, NULL, NULL, NULL, 0, 1539848063, 1, 1, 1, NULL, NULL, NULL, NULL, '4b8b6babd0aaf04fc19025a3486ee6cd8ca8caf7', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(92, '', 'moly9250', 'd60dacad426defdcfef9545245617812', NULL, NULL, NULL, NULL, 'euijuooo@gmail.com', '0xf89260db97765A00a343aba8e5682715804769ca', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'cb6bbd761929587e8e4d7c6623aa7bdfed9dc2c4', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(93, '', 'Ricky', 'fa5c318914bced8815df5e906fd957ea', NULL, NULL, NULL, NULL, 'ricky_holis@yahoo.co.id', '0xa58f0F907C1a004C0d9A89636bF370fbA6eD898d', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '5492c2219b2906251c1f28309c0084d6ef187fed', ''),
(94, '', 'Bearwife', '811466bc0d24846cf09823936d39ccc9', NULL, NULL, NULL, NULL, 't01056037444@gmail.com', '0xc71c331c6E565EDF857Cf8A20AD45fda79c31349', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '26e8b8c3b2b524a8ddf063d7f3441b1eff9a983a', 'a55ade8ca4daa775e605e11a17efccbb5e3a4dfd'),
(95, '', 'Jeff', '0f091cc5d7d732720300fedd3d1db63f', NULL, NULL, NULL, NULL, 'flyersfreak88@gmail.com', '0xfb84cb9ef433264bb4a9a8ceb81873c08ce2db3d', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '87a23134cc6189cc01b9d8b7ee5842ad3d1539b0', '6bbe668e4a9f5f66f0e75ac34fc400f90b0fb6d6'),
(96, '', 'Mohit', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, 'mohit.brinfotech@gmail.com', '0x3a1e9200c3463ec01848da266da3bbce98ad8e60', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'e5a588c569216fa32e87a28910d0e1b992e63bea', ''),
(97, '', 'Adam', 'e9eab499a13a3482b304cb0cb80af8c1', NULL, NULL, NULL, NULL, 'mon1967@naver.com', '0xe24327D1dfD178633d636e9D2A9cb3e378EE6B39', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '4c42c7b8610646bc4789f87aae3f0d9e4228d258', '4b8b6babd0aaf04fc19025a3486ee6cd8ca8caf7'),
(99, '', 'coinyoung', '747e3012f0d10d71182faf2932be2630', NULL, NULL, NULL, NULL, 'lyw030303@gmail.com', '0x9B233800D7F352b886A353Fd56Bac3121f804f59', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'ab3fbe14dbc5f6013942b75d9afee44df0f349d2', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(101, '', 'sember', 'f570c09a73ddaec89f88d01ad64a8441', NULL, NULL, NULL, NULL, 'haopard@gmail.com', '0x793E764947624058B434B98c929FECd3C770d53C', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'da7ad6df309978e8af8965960aa79e6a3835c03e', 'a55ade8ca4daa775e605e11a17efccbb5e3a4dfd'),
(102, '', 'sumin', 'd4e41750f3ee86db38c5dc1572c11f3d', NULL, NULL, NULL, NULL, 'nenesu@naver.com', '0xD2Aa80BCFCA35d0f398479200793C484C135e9Cf', NULL, NULL, NULL, NULL, 0, 1539513075, 0, 1, 1, NULL, NULL, NULL, NULL, '543d308576363db2a2f63dbdefd751cd772f3b31', '4b8b6babd0aaf04fc19025a3486ee6cd8ca8caf7'),
(103, '', 'Justintheelite', '17789dabee2d0f42545a05ce52873f25', NULL, NULL, NULL, NULL, 'chimberle@naver.com', '', NULL, NULL, NULL, NULL, 0, 1539866150, 0, 1, 1, NULL, NULL, NULL, NULL, 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2', '0'),
(104, '', 'Caterham77', '2c9480aa12f4be882dfb2fd5fd3043c9', NULL, NULL, NULL, NULL, 'willie.pieters5@gmail.com', '0xcCbD83e519BD2094a4329887b7CA31EeCe63FCcc', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '7e71fc94601c2fc561f1896ba7efb025f79c7039', 'a9592f622812af0d41cccfe116ad04264b90869c'),
(105, '', 'brother429', '3442a3a17b3612836e119d3a91ed2c30', NULL, NULL, NULL, NULL, 'lightman6002@gmail.com', '0x78d367333a707C8F19F8F2180E2a5410516F2d7f', NULL, NULL, NULL, NULL, 0, 1539480038, 0, 1, 1, NULL, NULL, NULL, NULL, '698000c569019a3886437fb62a4618a4d906b80f', '4b8b6babd0aaf04fc19025a3486ee6cd8ca8caf7'),
(107, '', 'bharat23', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, 'bharatchhabra13@gmail.com', '0xf0b9828d5fa4fc6fe5525c69f088d34e4101e385', NULL, NULL, NULL, NULL, 0, 1539439027, 0, 1, 1, NULL, NULL, NULL, NULL, '9ab8028f89129b771c9a94d4d021fcb6ca72d7e3', ''),
(108, '', 'karan', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, 'karan.brinfotech@gmial.com', '0x32Be343B94f860124dC4fEe278FDCBD38C102D88', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '29faf475d8f33ac89db5adc67c1af7cc79edfa2f', ''),
(109, '', 'karan solanki', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, 'karan.brinfotech@gmail.com', '0x32Be343B94f860124dC4fEe278FDCBD38C102D92', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '23aa3f10aedbf84b6448aefc9f7af1690ce6c25b', ''),
(110, '', 'Karan S', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, 'karansolanki1310@gmail.com', '0x32Be343B94f860124dC4fEe278FDCBD38C102D90', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '8fe4faf8ab821d01c463e776294e29c38715cf87', '23aa3f10aedbf84b6448aefc9f7af1690ce6c25b'),
(112, '', 'Kimgp0507', '7c74661bfa7ee9cd2a9ca14cb9e61ce1', NULL, NULL, NULL, NULL, 'kimgp0507@gmail.com', '0xa49D6608d746891854D3030BCE834b1f74D35CE8', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '1d7cfd5ce9687754aa880f2d8596f2f40984a85d', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(113, '', 'JO KYUNG SOON', 'b8e7f6d5f87e3902dbde1b2d9e53acaf', NULL, NULL, NULL, NULL, 'kyung4459@gmail.com', '0x3f8c3c480c036b1de9b960b9300247b5d0aab923', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '352c9c61835491cb775b3c14df45d3deee3f9753', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(114, '', 'Golden Yogi', '0766a0734b2ceeef279c9a9b0434f6ab', NULL, NULL, NULL, NULL, 'thegoldenyogi@gmail.com', '0x703b16787180a94c2f9f2510F08eDB59Aa899568', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '86b5cf2b6c0db75ce44632e659c952bd6b6da4ae', ''),
(115, '', 'No1CryptoFan', '7cb5416c74e45a9e7411d29fbde0158e', NULL, NULL, NULL, NULL, 'geldmakenmetcrypto@gmail.com', '0x931d1E678E4ff13C2b80a945b87aAf62F87bdef2', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '714eff979276ebf18c7f2baaa8bd2c6913809054', ''),
(116, '', 'reggiechoi', 'e91e93b4f105f02c688daf952da6aae8', NULL, NULL, NULL, NULL, 'reggie3237@hanmail.net', '0xae99806301B52241a85353Da44667D4Ca881a75e', NULL, NULL, NULL, NULL, 0, 1540004285, 0, 1, 1, NULL, NULL, NULL, NULL, 'ff97db849801e41890690dbbe22c63938405d3c4', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(117, '', 'BlackKrakenCrypto', '57fc687d6d3db34f3925c6cf1397122e', NULL, NULL, NULL, NULL, 'bkc2000@protonmail.com', '0x12d2412f14a869f251b7f1c77c697c8f5fbd42b5', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '8889cd659c6735447e5f3bd4356dd1151f513172', ''),
(118, '', 'jadekang', '6226d031c517b50eb368bd2b15a9d504', NULL, NULL, NULL, NULL, 'rolex0326@hanmail.net', '0x9ca94ceca50759d079625fdec7dc39601f058867', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '2ba60d57e1cd8ee0c704ef4405090603aff07988', '698000c569019a3886437fb62a4618a4d906b80f'),
(119, '', 'hsy', 'ada15dbbb723e4df5751312007fd25b5', NULL, NULL, NULL, NULL, 'toji.9999@daum.net', '0x79edE314367124506903c10649a487cc6837AB36', NULL, NULL, NULL, NULL, 0, 1539740589, 0, 1, 1, NULL, NULL, NULL, NULL, 'c08f7aece9b16e9f5b986c91aa1553e806ca63ae', 'ff97db849801e41890690dbbe22c63938405d3c4'),
(120, '', 'yobo', '5887132b02a15f351d772c9adfbb3008', NULL, NULL, NULL, NULL, 'bobo9992@gmail.com', '0x190a2409fc6434483d4c2cab804e75e3bc5ebfa6', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'b3a0d8d1b0f916ad598c7b63491bdb2c3db32bb1', ''),
(121, '', 'Nicholas Tacminzis', 'f77605c7396fc5ec036892b144cd656f', NULL, NULL, NULL, NULL, 'nicholas@tensai-solutions.co.uk', '0x17890801e3f3cdd50f50cda75aa200e3ebafa145', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '249b8fb0790173886dc7fbd67ae05d23216a10ee', ''),
(122, '', 'munyoung', 'd54b0e480a363f979c5e32bf21275459', 'Fga0Ngfr8xtURIdXr0Ekg50sUQsul/xGIcO1yHifQBYY4Rl5cgB0BGJnQ9H8pjS2dKOaZ84G1y2Qbb4zLktRSQ==', '1cAt0EQxiLqQGh493Ek1yO7cb6a54d0191e99aab', 'Ri1V6E1PxkZ+27TJ4qlIv4Q9+zT/zhVbA+KT8SZeRpObNqw7xVbd0Dc4Z3lbFHcHPTpnZaCPQlNFroaw5TeBFOYzGgX0y1TorM/KGVlD+xyAe+9bCL9RtjR5SKgI+uPV8F9OhWhdiboKTPDjW7K6dI/P+ms3nQLwU9Z/I3yy41igwST8oQBtFSqtMa0P6WiItGXb/U9W9CgTRIbGWeghwMZaNtRDWlVDyMSszTL/3YEVc1iInnRzWEmsPnIw10Iw0FpBIdB/OrVcVez+J27uRqKfuJ8fLxv9zBktWEl2RkP+Lb/D11nhaFuMNhFOWIKA3kd5yP/AC6bW+qZrTETCnnHlPbuPLtjf6tc6plsQfUCsRPvg5m1IA1OPj4A5wHLz', NULL, 'd9jmy2567@gmail.com', '0x467045F67b5306fAbb30a9574dDee07c08d723F0', NULL, NULL, NULL, NULL, 0, 1539494005, 1, 1, 1, NULL, NULL, NULL, NULL, '6fa8073a211567b57b562487c0f7356c2879d455', '4b8b6babd0aaf04fc19025a3486ee6cd8ca8caf7'),
(123, '', 'sunhee', '66338d5248d9accf1f0e4a78bcfa3e9c', 'lkviYD+C9KFGeH4uALcGGwGU2aGL3uhgEZ3d22vnFW14ypBsqA4V49AWKnodp+eH7M3Y426bsLgClTDQeoHQrA==', 'V5WJUjo3cU8aZutlOYIih.2f19188a04a5391e0c', 'Ssn36WmtmTimFqVrLGXxMu9wjQjP+GKU0ejfpj3S0Zdut5qDn5vWwrGEzVKdreQY9nWqlrvOQuKPxmPzxs76IfT85CQIomn+JbQ5Pkc6ESRiph/FK/hRnLWaR/RXN5K30QEOuotn7Sh4XSuhQkqX616rlq86l20qsWrxIJLPZ+QmvQ4wr78H0u44jcX85t/bKSFZMEgXYYzy7SE5SuSdmd+NaQIlsk1MEVJrCKMzPB1wCumIwcUdyyX96k9bgbwqE+OsRAS61K0AGMw+SEPYrnSTr1vg+BYatkYJi4aJCTh9UNcPmfeXk3Xcr5Dej8DUe82inTtbXqCqfSa/njLjLYE0YmMNKNnRHZXkZ2e6nRtXcjcjrOQxeIZwCU7Qbdpv', NULL, 'yoo85082352@gmail.com', '0x1036917673df1E01E9d39A1E1A56cf18871c66fD', NULL, NULL, NULL, NULL, 0, 1539491754, 1, 1, 1, NULL, NULL, NULL, NULL, '7f3983558f1ab0ed342acdb6e3c9eecb270aac87', '4b8b6babd0aaf04fc19025a3486ee6cd8ca8caf7'),
(124, '', 'Hansu Na', 'c60c563848cf92a7ed85c35e48ef33b3', NULL, NULL, NULL, NULL, 'nahs1128@naver.com', '0xA060c57b0DD736403210775210998c346c4F2A58', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '41e741588c7c7dd803d0b62b0a72eac6db0e51a0', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(125, '', 'Sanghee Lee', '3ee3e429d9a829324a137722e0af7099', NULL, NULL, NULL, NULL, 'h340@naver.com', '0xae389055a0619979779F112E1aa105dd4577376E', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '79b0e3bf8018be88236da947f3e3c73c4017e07b', '41e741588c7c7dd803d0b62b0a72eac6db0e51a0'),
(126, '', 'Richard Nacht', 'b0e58c459bbb2adf9735dea4f93ee69f', '2lKZab9eXT0G8mf6ApdXg4vJIUXPs3xfw4Q5cWHnI9mLLQGHehpkGy3tNxr14g7EAbCS43PfwfBoWjSS7erhYQ==', NULL, 'q/5ANSCRNR9+DDBt/cqP7BAjdAlOmrtQH4H1PBGsXVFXEsIITUD96FDO+EDUUYX2juqAGdwXOtE9vsFYe0visE9cbtRzUzEQYC6hTtCiUen0h4nas0hyROx160ViiDV2W5Oli0wzCCmuniPtwwpgVQ9u+8WXwzm8kGFmgbi7n6T2j4tuM9ctais7Q/yFHCAszgFYlN/IjvdoGQrjYkFNQc12374jWDNfBRrjWnlwbJBwuZ4zrfNtXXCSIiNr6ZsH1oNW7n19vWBGRvCUi7ACFlXrFjgHngymXfKK5zsNzHTmIm7r6OXVhtbqZsf3IfV7wYQ+3UkqI4gIDzUBj2G4JXt3tRoeR7M81/wc65Pr18Fr0P9gk8uQtZlSSs1DJ5Le', NULL, 'rnacht@gmail.com', '0xdB88550880B322BB0818bEC45abe7490Ad7456Ba', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, '38eb2e5a3755791c3129d60a23411d980076b00c', ''),
(127, '', 'vlad', '4ba93687408f904a36b993a082360c1e', NULL, NULL, NULL, NULL, 'sergeym610@gmail.com', '0xf1c1E0838F8AC06AEC963355E0ee945765599e23', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '0cca1d783afc3809b0a8115a792bf6619e058b85', ''),
(128, '', 'CRYPTO DAD', 'f7ccfe683f3891b6c50d802f5b9b33e3', NULL, NULL, NULL, NULL, 'exc0t8w@gmail.com', '0x5547DcE645750006C5d92d0579ce9CED0355324c', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '0524d9f7b8765fbacb9205d133e0a699f5be2ee1', ''),
(129, '', 'seunghohan', '43311eb6e1af532e30db455af34c29a4', NULL, NULL, NULL, NULL, 'zhdkffk4265@naver.com', '0x2c4b2d9d32eacea05cfd24231991a6d9a026cfb6', NULL, NULL, NULL, NULL, 0, 1539528500, 0, 1, 1, NULL, NULL, NULL, NULL, 'e976c86919c4eafffb1d592717ad184dc1ca91b9', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(130, '', 'Alvin Senjaya', '37766a6c952551222ba2bd99e1205285', NULL, NULL, NULL, NULL, 'alvinsenjaya@ymail.com', '0x03ce6590ee9e647744D563f459c2b16F276a8A57', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'b5858f8cb8d2345b66e40dcf437efde279cabf06', '5492c2219b2906251c1f28309c0084d6ef187fed'),
(131, '', 'Jjdigger', 'bdb1fff4cdceb3dccd803bd56cb23f54', 'oaqP7p7NlztfplKV+driCeuST07jPMFmr3pjmiRLb+it5+3QUCaf5XeoSdoiP4Z9Fv73Tiu4hLmImUM/gZxi4g==', NULL, 'SFNV4ees5SdGF/FxFIQksxWBMj/1oCINIEuB7gKWlZwuztHJ68NHRYo0VoegZah7QZSKBMZ3YsmyG3ewxo+ZMEeKI82nFNXHIUB4m1uNHAia9wmw8eQcbUDJ1v1qTHJXeChZuS61idzkb0aMsoA6hZqtUK/ViO3qe1/bQkcokaaE9eZu/HFZHJ1dSjl8UEavOqoQi1nYh6Dmv/obxx3oBy6NclyqUuDaDDONNIPWKM4XQGRmStB2EFN007IXASkci1ZHtBw0oxeFGsp2Oj/ctQDa/3VXpzbVpu0emaJLUAN8iUibdn5O4Hk6PiUI10gG99FwhxDdc8LPPfOjH/l/lBPxaITGNcMdnH1A6iGuw1WVWNBBJ0fAMz3cdihm0Qaa', NULL, 'jimohgarba77@yahoo.com', '0x4914eeA84dF07abe7400380A59bF82800Faad2F1', NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, 'df53b3989d77825d95fd9067303e311ca4373dce', ''),
(132, '', 'Yemmey', '50995f4f774ec5419d89483309bc4761', NULL, NULL, NULL, NULL, 'lawaladeyemi@gmail.com', '0x1ecaacdc9d4fb3c5d3b23b173e43de363b34d65b', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '9e60014368f07d2dc1528934524816ce0d21d43a', ''),
(133, '', 'wongsableng', 'e05fcb51a80f1b5a0c73d0cd9ca86e09', NULL, NULL, NULL, NULL, 'saya@wongsableng.com', '0x549956701A4624EE627926BDc6EeE7E8Af02DD6b', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '32175f495ad8f8a64113696a4c08f66c70a6c89c', ''),
(134, '', 'vo thi thanh huyen', '10626f81851d2345b828f8754dc7ea58', NULL, NULL, NULL, NULL, 'thanhhuyenht1809@gmail.com', '0x53a2c57DD53c1aa1036cC9D992a725F847614181', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'ad305e3a6ba93423d181a93544c2860e4bd99a57', ''),
(135, '', 'sunghot', 'f7136d06b4e0139e7d6211188a40c0ef', NULL, NULL, NULL, NULL, 'sunghot1@naver.com', '0xAbad0Fc0b6eD2E0A39bd20d1Ff3e387b7bB257B4', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '21ca1df72e5f37a52230471d79ad35312ae64f93', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(136, '', 'amitsharma', 'c8bc7ed03e19e625fefedbde8d78f4ce', NULL, NULL, NULL, NULL, 'as.9075@gmail.com', '0x421D7643Cf71b704c0E13d23f34bE18846237574', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '8f31767206c20647fd41ca2c6c01304e16e08152', ''),
(137, '', 'Twysted Rei', '0ea39011b3e221783942f62ff71e781c', NULL, NULL, NULL, NULL, 'h01d10.96@gmail.com', '0xdadF9Ef0c22B41c2378895fbef1c593A64b2B44C', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'eb24948e0bcb77bbb425ba54ec11fde5f36ea1d3', ''),
(138, '', 'King Fomo', 'b3a639210de1e7dc5a358dc9b48ec4c2', NULL, NULL, NULL, NULL, 'Dailycryptoupdate3@yahoo.com', '0x4ffE17a2A72bC7422CB176bC71c04EE6D87cE329', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '66ef9ad1d957c544e17964482c293b2b49020c62', ''),
(139, '', 'Smurf', '049b5892c03b73ba13cc7aa9325f6eb0', NULL, NULL, NULL, NULL, 'smurf-sam@hotmail.co.uk', '0x6Ed450e062C20F929CB7Ee72fCc53e9697980a18', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '685a1d34d92c2a5677094fb8c6ed4913d31f5789', ''),
(140, '', 'dandyfe', '123f7593fbb29f783340f5bea137fd84', NULL, NULL, NULL, NULL, 'lim5397@nate.com', '0x660bf036a94ddfcd71c7ee02a220fa7b81b65aee', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'dabb60eb155ce3d9eb6eb6e64d49974bbb0e45e2', ''),
(142, '', 'sempee9', '57a455ca5daa67a8f2980d61d11cc718', NULL, NULL, NULL, NULL, 'sempee9@naver.com', '0xd0852186ee416bfd71feb2a84b23b263e166b7cc', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '6dc92c59378f16ea9a2a7828178d350f1b056d7d', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(143, '', 'Xrypto', '05b94f751172e6ec69e5f1f3d98ba01a', NULL, NULL, NULL, NULL, 'jkuba1275@gmail.com', '0x797452296a37C2A8419F45A95b435093917f8f8B', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '45875f8668c5ce8c4ef6502bdf9a66600e367e22', ''),
(144, '', 'Pavel Reznikov', '806595eeeea1c7bb3453d803b4ce9493', NULL, NULL, NULL, NULL, 'pavel.reznikov@gmail.com', '0xf023445bd177aeaa2d8816e47d36cd39b40efe63', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '2b077c9c59a7af8ca7ae130319634670a1da06f9', '3ecfb8a8f5ada39497b1ca4358d1c47311b8055d'),
(145, '', 'hongsungjin', 'b27fd4c934f23d3ac69cb9a7a516565b', NULL, NULL, NULL, NULL, 'haj3416@naver.com', '0x7E610486689F894169833B75F81fE986271A6916', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '9cefb5a36c015a626f9d60522b17ffafd7f57a67', 'ff97db849801e41890690dbbe22c63938405d3c4'),
(146, '', 'hongsungjin', 'b27fd4c934f23d3ac69cb9a7a516565b', NULL, NULL, NULL, NULL, 'hsj3416@naver.com', '0xCbF101bAc37a2A83225Dc00fA9B708a8bA87Da5C', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '82a727396b26d6f2551e15cd76eae5e002d08f2b', 'ff97db849801e41890690dbbe22c63938405d3c4'),
(147, '', 'Park Chan Young', 'f93451633ee17c481f82ffb961b04aad', '3DuWpjLkswEbuEcea5NNE7Cwh1nlS/n7/PcH3SFBAm32iU+uKat/qopCsZ/kmRgAoNwdVZaK66iY+h1yp3WDYg==', '4AkxsTOO0SWdHNe/zgwJU.247d57e31e5bceb806', 'k2CukrhzjkJ4qfDc6HCa/o8+ErDuY1z64HY1GeDOGzgqlXQBNzHHkZ+F3MNpWa4nNB2XO5f7au64HBTAd4JwkIzvTmBvq/JVfKSdrHo00BARYcqFDhZRrRMTzKFxFY6tYqmpS0KXepWQy5coHXJ5F0bTV2OfYhAADHKEdO61Up3rkGcTAaQet22uqsoopHxRpT/sNWkFLjbMxepLCE72gMSXppA5xtRnNgxdXb2qX38KZSWLAB099G69XXed6mBELK1lPsM7nThsW16TAoYermPCjht9MKX/BnquCCk6jJfE7PNm20uGBMEAKgEkjEt6RD/u078g3h6TZgyF9s4EStYRGz6VMcioVNxVyovwmS93QlhQfPt27+g/UFV56/TV', NULL, 'gksmf5404@gmail.com', '0xaca6822c084bcd91fe6d7eefafe1ac8a358ca05d', NULL, NULL, NULL, NULL, 0, 1539701311, 1, 1, 1, NULL, NULL, NULL, NULL, '56b32cb641c8e67eae033ea24167a1997271d574', 'c8088b56a84f1fa25a3c82e2eb47ecb0ea00d3d2'),
(148, '', 'cryptogor', '998fb3c39bd56459f8f1c8b55c9b03d1', NULL, NULL, NULL, NULL, 'cryptogor@gmail.com', '0xD117Ec705e6A0ef33326a703c07f3d267d5BBeF3', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '43d1cfa1656e46360473e1795bb1a290c5be5275', ''),
(149, '', 'DamianK', '5a2c590be2b1d72adb7831c90f1c91c1', NULL, NULL, NULL, NULL, 'damjan.kragolnik@gmail.com', '0x750E473fa6C37Cb9D5Fc2a41fb40447De1e950d1', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '28e9818ffc32343392e6e913483367c01a6150f8', ''),
(150, '', 'junhycklee', '7128eb012b2ede1c30bd5c363414c78b', NULL, NULL, NULL, NULL, 'qlxm010@hanmail.net', '0x3Ada24Ba885Fde09E347daFa83D6C0Ccd58ff3d8', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, '3af9ddba50a554ea53848ce981955d90431e7826', '4b8b6babd0aaf04fc19025a3486ee6cd8ca8caf7'),
(151, '', 'lchlch22', '44068e9d063009da5b7ac9865855ea2d', '8JCNU5EiXhxHAH5jkrh9E66nxd2LIhnVFoR9vlg8kdPpV/vUjQ3SW7GFtnJbqhJSEAdydj/9vQ3FBbKjWwiLzA==', 'IcdL1MLh.6y4gKJBvH6HXO572718aad723be2072', 'KtJVpBfFqfNsh/a1my08bSW5UEZrqrX0gvcO4EYZbb3FDSgTJJNVnMeVBDYKfb8eXdWSozchuoi79ACAdG9R8H0P1JqiMT2DHKw63iJLLoQmweksln7HvftxwWU3sdSCKl/rygXRQMgHso1j5ySyd2rHF9VFgbmAj0X1e36EXhY9wkaWOZZY1yRz6o4qtPNjbiyAopbtg/6cUvm+HQNf/ZMCmZ/HDM14WEB2fqvKoRva8CDIQ74scXlJgdIsLVU4UTkDtzRo01wftUHGaG0rzWqzCu7zeG86aosXTaKQ219tJDZfqj8d1uflDTq5Z5GvYWIrar7UtWSMABJB8ZfSq67wIy2S8okXjDHHPecR7q8WO1DKBg6LeAsibax4PKpg', NULL, 'lchlch2222@naver.com', '0x67910513cDF094af09fB64Cf809F6239FAFa9A5c', NULL, NULL, NULL, NULL, 0, 1539753822, 1, 1, 1, NULL, NULL, NULL, NULL, '724b6d73e6c020ea891664c9747502395009dd52', 'c08f7aece9b16e9f5b986c91aa1553e806ca63ae'),
(152, '', 'kyunghee  hyun', '1f0e1203d0211271891ba9b2597ba013', NULL, NULL, NULL, NULL, 'qaz1224@daum.com', '0x7E28e62387007f2e73df874a3528D145c1Ac7e89', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 'c238b8ae0ab6f0379a906500128d2e40e08b96fc', '3369b8c0d6667a1b8fbd99fbdf36d2a5f96e999b'),
(153, '', 'Karcar23', '54512c4c6d03f9e3f9c90bd048226ac8', NULL, NULL, NULL, NULL, 'Kareem.carzan23@gmail.com', '0x1B52DfC0651e65F4218BB1039573faB6B25c3D68', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, 'aaa5cc0ab424ff55434eafbddedd0a519148ef26', '3ecfb8a8f5ada39497b1ca4358d1c47311b8055d');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_bonus`
--

CREATE TABLE `user_bonus` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `referal_user_id` int(11) NOT NULL,
  `referral_user_coin` varchar(1000) DEFAULT NULL,
  `user_bonus` varchar(255) DEFAULT NULL,
  `referal_user_email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `modify_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_inquiry`
--

CREATE TABLE `user_inquiry` (
  `id` int(11) NOT NULL,
  `order_type` varchar(50) NOT NULL,
  `currency_from_to` varchar(100) NOT NULL,
  `price` varchar(50) NOT NULL,
  `api_name` varchar(100) NOT NULL,
  `order_book_price` varchar(255) NOT NULL,
  `mix_order_book_price` varchar(255) NOT NULL,
  `calculate_price` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_inquiry`
--

INSERT INTO `user_inquiry` (`id`, `order_type`, `currency_from_to`, `price`, `api_name`, `order_book_price`, `mix_order_book_price`, `calculate_price`, `user_email`, `status`, `created_date`, `modify_date`) VALUES
(1, 'Buy', 'BTC_USDT', '0.01', 'BINANCE', '63.98030000', '63.97087629', '0.00942371', 'bharatchhabra13@gmail.com', 0, '2018-08-16 04:31:54', '2018-08-16 04:31:54'),
(2, 'Sell', 'ETH_BTC', '0.01', 'BITTREX', '0.00046100', '0.00046104', '0.00000004', 'bharatchhabra13@gmail.com', 1, '2018-08-16 04:46:06', '2018-08-16 04:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `value_and_bonus`
--

CREATE TABLE `value_and_bonus` (
  `id` int(11) NOT NULL DEFAULT '1',
  `evot_value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bonus` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modify_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `value_and_bonus`
--

INSERT INTO `value_and_bonus` (`id`, `evot_value`, `bonus`, `modify_date`) VALUES
(1, '0.45', '5', '18-10-2018');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
  ADD PRIMARY KEY (`forgot_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_chat`
--
ALTER TABLE `support_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_sessions`
--
ALTER TABLE `sys_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `user_bonus`
--
ALTER TABLE `user_bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inquiry`
--
ALTER TABLE `user_inquiry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forgotpassword`
--
ALTER TABLE `forgotpassword`
  MODIFY `forgot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `support_chat`
--
ALTER TABLE `support_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_bonus`
--
ALTER TABLE `user_bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_inquiry`
--
ALTER TABLE `user_inquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
