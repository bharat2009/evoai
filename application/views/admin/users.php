	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Users
				<small>Control panel</small>
			</h1>
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i>Dashboard</a>
				</li>
				<li class="active">Users</li>
			</ol>
		</section>
		<div id="msg_div">
			<?php echo $this->session->flashdata('message');?>
		</div>
		<!-- Main content -->
		<section class="content">                
			<div id="content">
				<div class="row">				
					<div class="col-md-12 column">				
						<div class="box box-primary">
							<div class="box-header">
								<h3 class="box-title">User details</h3> 
							</div>							
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Email</th>  
											<th>Price</th>  
											<th>From to</th>  
											<th>API name</th>
											<th>Order book price</th>
											<th>Mix Order book price</th>
											<th>Calculate price</th>  	
											<th>Status</th>  	
											<th>Action</th>  	
										</tr>  			
									</thead>
									<tbody>									
										<?php 
											if($user_list) 
											{
												foreach ($user_list as $row)
												{ 
													?>
														<tr> 
															<td width="10%">
																<?php echo $row->user_email; ?>
															</td>
															<td width="10%">
																<?php echo $row->price; ?>
															</td>
															<td width="10%">
																<?php echo str_replace('_','-',$row->currency_from_to); ?>
															</td>															
															<td width="10%">
																<?php echo $row->api_name; ?>
															</td>															
															<td width="10%"><?php echo $row->order_book_price; ?></td>															
															<td width="10%"><?php echo $row->mix_order_book_price; ?></td>															
															<td width="10%"><?php echo $row->calculate_price; ?></td>													
															<td width="10%">
																<a href="#" id="active_<?php echo $row->id;?>" <?php if($row->status != 1){ echo "style='display:none;'"; } ?> class="btn-group" onclick="return setStatus(<?php echo $row->id;?>,'<?php echo base_url();?>admin/users/setStatus','0')">
																	<button class="btn btn-sm btn-success">ON</button>
																	<button class="btn btn-sm btn-default">OFF</button>
																</a>
																<a href="#" id="inactive_<?php echo $row->id;?>" <?php if($row->status != 0){ echo "style='display:none;'"; } ?> class="btn-group" onclick="return setStatus(<?php echo $row->id;?>,'<?php echo base_url();?>admin/users/setStatus','1')">
																	<button class="btn btn-sm btn-default">ON</button>
																	<button class="btn btn-sm btn-success">OFF</button>
																</a>
															</td>
															<td width="10%">			
																<a class="confirm" onclick="return delete_detail('admin/users/delete_detail/<?php echo $row->id;?>');" href="javascript:void(0);" title="Remove"><i class="fa fa-trash-o fa-2x text-danger" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>	
															</td>
														</tr>  										
													<?php
												} 
											}
											else 
											{
												?>
													<tr>
														<td colspan="10">No Records Found</td>
													</tr>
												<?php 
											}
										?>
									</tbody>
								</table>
							</div><!-- /.box-body -->
							<!-- /.box -->
						</div>
					</div>
				</div>
			</div>
		</section><!-- /.content -->
	</div>
	<script>
		/* Change status */
		function setStatusDoc(ID, PAGE, status) 
		{
			var str = 'id='+ID+'&status='+status;
			jQuery.ajax({
				type :"POST",
				url  :PAGE,
				data : str,
				success:function(data)
				{			
					if(data==1)
					{
						var a_spanid = 'active_d'+ID ;
						var d_spanid = 'inactive_d'+ID ;
						if(status !="1")
						{
							$("#"+a_spanid).hide();
							$("#"+d_spanid).show();
							jQuery("#msg_div").html();
						}
						else
						{			
							$("#"+d_spanid).hide();
							$("#"+a_spanid).show();
							jQuery("#msg_div").html();
						}
					}
				} 
			});
		}
	</script>