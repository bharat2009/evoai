<div class="dash-body">
	<section class="header dash-header" id="top">
		<div class="container-fluid">
			<div class="header__logo pull-left">
				<a href="<?php echo base_url();?>">
					<img src="<?php echo base_url();?>evoai/public/webroot/frontend/images/logo_web.png" alt="" class="logo_pc">
					<img src="<?php echo base_url();?>evoai/public/webroot/frontend/images/logo.png" alt="" class="logo_mob">
				</a>
			</div>
			<a href="#" data-toggle="offcanvas" class="toggle-btn"><i class="fa fa-navicon fa-3x"></i></a>            
			<!--<a onclick="" href="#" target="_blank" class="btn btn-danger header__contribute">CONTRIBUTE</a>-->            
		</div>
	</section>       
	<section class="dashboard-section">	
		<div class="wrapper">
			<div class="row row-offcanvas row-offcanvas-left">
				<!-- sidebar -->
				<?php echo $this->load->view('sidebar.php');?>
				<!-- /sidebar -->
				<!-- main right col -->
				<div class="column col-sm-9 col-xs-11 main-dashcontent" id="main">  
					<div id="msg_div">
						<?php echo $this->session->flashdata('message');?>				
					</div>
					<h2 class="pro-heading">Wallet	</h2>
					<div class="row clearfix">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="" class="form-control" placeholder="Cryptoguy">
							</div>

							<div class="form-group">
								<label>Email</label>
								<input type="text" name="" class="form-control" placeholder="Cryptoguy@gmail.com">
							</div>
							<div class="form-group">
								<button class="btn-submit btn-update mt-0">SAVE</button>									
							</div>

						</div>	
					</div>
				</div>
				<!-- /main -->
			</div>
		</div>
	</section>

</div>
