	<section class="header notfixed" id="top">
        <div class="container-fluid">
            <div class="header__logo">
                <a href="<?php echo base_url();?>">
                    <img src="<?php echo base_url();?>evoai/public/webroot/frontend/images/logo_web.png" alt="" class="logo_pc">
                    <img src="<?php echo base_url();?>evoai/public/webroot/frontend/images/logo.png" alt="" class="logo_mob">
                </a>
            </div>
            <a href="javascript:open_popup('#popup-menu')" class="header__menu_mob_link"></a>
            <nav class="header__menu hidden">
                <ul>
                    <li><a class="nav-link js-scroll-trigger" href="#ecosystem">About EVOAI</a></li>
                    <li><a class="nav-link js-scroll-trigger" href="#apiConnected">Partners</a></li>
                    <li><a class="nav-link js-scroll-trigger" href="#ourTeam">Team</a></li>
                    <li><a class="nav-link js-scroll-trigger" href="#distributionTkn" target="_blank">Token</a></li>
                    <li><a class="nav-link js-scroll-trigger" href="#roadMap" target="_blank">Roadmap</a></li>                    
                </ul>
            </nav>
            <!--<a onclick="" href="#" target="_blank" class="btn btn-danger header__contribute">CONTRIBUTE</a>-->
            <nav class="header__right">
                <a href="#faqs" class="nav-link js-scroll-trigger">FAQ</a> 
				<?php
					if($this->session->userdata('Ref_UserID'))
					{
						?>
							<a href="<?php echo base_url('home/logout'); ?>" class="header__wallet">
								<span class="text-center">Logout<span>
							</a> 
							<a href="<?php echo node_url; ?>/dashboard" class="header__wallet">
								<?php echo $this->Ref_UserName; ?>
							</a> 
						<?php
					}
					else
					{
						?>
							<a href="<?php echo base_url(); ?>login" class="header__wallet nav-link js-scroll-trigger hidden">
								<img src="<?php echo base_url();?>evoai/public/webroot/frontend/images/login_new.png">Login
							</a>                    
						<?php
					}
				?>
            </nav>
        </div>
    </section>