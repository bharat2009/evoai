<div class="column col-sm-3 col-xs-1 sidebar-offcanvas" id="sidebar">
	<ul class="nav" id="menu">
		<li class="<?php echo ($this->uri->segment(1) == 'dashboard')?'active':''; ?>">
			<a href="<?php echo node_url ?>/dashboard">
				<i><img src="evoai/public/webroot/frontend/images/icon-home.png"></i> 
				<span class="collapse in hidden-xs">Dashboard</span>
			</a>
		</li>
		<li>
			<a href="<?php echo node_url; ?>/wallet">
				<i><img src="evoai/public/webroot/frontend/images/icon-wallet.png"></i> 
				<span class="collapse in hidden-xs">Wallet</span>
			</a>
		</li>
		<li>
			<a href="<?php echo node_url; ?>/live_trades">
				<i><img src="evoai/public/webroot/frontend/images/icon-livetrade.png"></i> 
				<span class="collapse in hidden-xs">Live trades</span>
			</a>
		</li>
		<li>
			<a href="<?php echo node_url; ?>/evabot">
				<i><img src="evoai/public/webroot/frontend/images/icon-chart.png"></i>
				<span class="collapse in hidden-xs">Evabot</span>
			</a>
		</li>
		<li>
			<a href="<?php echo node_url; ?>/evobot">
				<i><img src="evoai/public/webroot/frontend/images/icon-chart.png"></i> 
				<span class="collapse in hidden-xs">Evobot</span>
			</a></li>
		<li>
			<a href="<?php echo node_url; ?>/eve">
				<i><img src="evoai/public/webroot/frontend/images/icon-chart.png"></i>
				<span class="collapse in hidden-xs">Eve</span>
			</a>
		</li>
		<li>
			<a href="<?php echo node_url; ?>/exchange">
				<i><img src="evoai/public/webroot/frontend/images/icon-exchange.png"></i> 
				<span class="collapse in hidden-xs">Exchange</span>
			</a>
		</li>
		<li class="<?php echo ($this->uri->segment(1) == 'profile')?'active':''; ?>">
			<a href="<?php echo base_url('profile');?>">
				<i><img src="evoai/public/webroot/frontend/images/icon-profile.png"></i> 
				<span class="collapse in hidden-xs">Profile</span>
			</a>
		</li>
		<li>
			<a href="<?php echo node_url; ?>/support">
				<i><img src="evoai/public/webroot/frontend/images/icon-support.png"></i> 
				<span class="collapse in hidden-xs">Support</span>
			</a>
		</li>
		<li>
			<a href="<?php echo node_url; ?>/announcements">
				<i><img src="evoai/public/webroot/frontend/images/icon-announce.png"></i> 
				<span class="collapse in hidden-xs">Announcements</span>
			</a>
		</li>                
		<li>
			<a href="<?php echo node_url; ?>/myreferrals">
				<i><img src="evoai/public/webroot/frontend/images/icon-referral.png"></i> 
				<span class="collapse in hidden-xs">My referrals</span>
			</a>
		</li>
	</ul>
</div>