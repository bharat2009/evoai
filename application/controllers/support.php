<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Support extends MY_Controller {
	function __construct()
	{
		parent::__construct();
    }	
	
	/*--------------------------------------------------------------------
 	*	@Function: index
 	*---------------------------------------------------------------------
	*/
	function index()
	{
		$this->data['title'] = 'Support';							
		$this->show_viewFrontInner('support', $this->data); 
 	}	
	
	/*--------------------------------------------------------------------
 	*	@Function: Store data
 	*---------------------------------------------------------------------
	*/
	function storeData()
	{
		if($this->checkfrantSession())
		{
			$postData = $this->input->post();
			$postData['user_id'] = $this->Ref_UserID;
			$this->db->insert('support', $postData);
			$lastID = $this->db->insert_id();
			if($lastID)
			{
				$msg = 'Details are stored successfull';					
				$HTML = '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div>';
				echo $HTML;
			}
			else
			{
				$msg = 'Data are not stored successfull';
				$HTML = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div>';			
				echo $HTML;
			}
		}
		else
		{
			$msg = 'Found some error in store data';
			$HTML = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div>';			
			echo $HTML;
		}
 	}		
}
